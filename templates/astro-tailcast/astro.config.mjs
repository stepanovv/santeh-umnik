import { defineConfig } from 'astro/config';
import react from '@astrojs/react';
import tailwind from "@astrojs/tailwind";

export default defineConfig({
  integrations: [react(), tailwind()],
  publicDir: '../public-static',
  outDir: '../../public',
  srcDir: './src',
  root: '.'
});